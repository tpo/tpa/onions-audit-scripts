This was used to audit the various onion services and see if there was
an authentication bypass. This security issue was reported in:

https://gitlab.torproject.org/tpo/tpa/team/-/issues/30419

I *think* it was used with:

    check-server-status.py < onionbalance-services.yaml

That magic file comes from
`/srv/puppet.torproject.org/puppet-facts/onionbalance-services.yaml`
on the onionbalance server (or the Puppetmaster)? More onion services
were extracted directly from the servers with Cumin, using something
like:

    cumin -o txt '*' "cat /var/lib/tor/onion/*/hostname" > all-onions.txt

And this had to be converted into YAML with something like this:

    sed -n '/_____FORMATTED_OUTPUT_____/,$p' < all-onions.txt | sed 's/^/{"/;s/:/": "/;s/$/"}/' > on.yaml

And then those services can be checked again with:

    check-server-status.py < on.yaml
