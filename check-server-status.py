#!/usr/bin/python3

import fileinput

import requests
import yaml


def load_onions():
    for line in fileinput.input():
        for hostname, onion in yaml.load(line).items():
            yield hostname, onion


def check_status(onion):
    resp = requests.get(onion,
                        proxies=dict(http='socks5h://127.0.0.1:9050'))
    return resp.status_code


def main():
    for hostname, onion in load_onions():
        print("checking %s (%s)... " % (onion, hostname), end='', flush=True)
        try:
            status = check_status('http://%s/server-status' % onion)
        except requests.exceptions.RequestException as e:
            print("exception %s" % type(e))
        else:
            print(status)


if __name__ == '__main__':
    main()
